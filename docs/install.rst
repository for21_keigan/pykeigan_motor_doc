==========
動作条件
==========
- python >= 3.5 (recommended) or 2.6
- pyserial >= 3.4
- bluepy >= 1.1.4 (BLE support. Linux のみ)


==========
インストール方法
==========
::

    pip install pykeigan-motor
    # BLEをインストールする場合
    pip install pykeigan-motor[ble]
