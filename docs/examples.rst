==========
Examples
==========
pykeigan_motor/examples/ ディレクトリにあるサンプルコードについて説明します。

USBのサンプルコード
-------------

.. csv-table::
   :header: File Name, Description
   :widths: 5, 5

   usb-simple-connection.py, KeiganMotor への接続と基本動作
   usb-rotate-the-motor.py, 指定秒数回転し、停止
   usb-position-control.py, 位置制御（絶対位置・相対位置）
   usb-get-motor.information.py, モーターの測定値を取得（位置・速度・トルク）
   usb-actuator.py, モーター測定値を使った往復動作
   usb-torque-control.py, トルク制御（回すほど重くなる）
   usb-teaching-control.py, モーションの記録と再生（ティーチング・プレイバック）

BLEのサンプルコード
-------------

.. csv-table::
   :header: File Name, Description
   :widths: 5, 5

   KM1Scan.py, KeiganMotor の MACアドレスを調べる
   ble-simple-connection.py, KeiganMotor への接続と基本動作
   ble-rotate-the-motor.py, 指定秒数回転し、停止
   ble-position-control.py, 位置制御（絶対位置・相対位置）
   ble-get-motor.information.py, モーターの測定値を取得（位置・速度・トルク）
   ble-actuator.py, モーター測定値を使った往復動作
   ble-torque-control.py, トルク制御（回すほど重くなる）
   ble-teaching-control.py, モーションの記録と再生（ティーチング・プレイバック）
   