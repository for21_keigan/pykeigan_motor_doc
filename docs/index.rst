.. pykeigan_motor documentation master file, created by
   sphinx-quickstart on Mon Jan 13 14:50:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pykeigan_motor へようこそ！
==========================================

==========
概要
==========
KeiganMotor を USB または BLE（Bluetooth Low Energy）経由で
コントロールすることができます。

- Github: https://github.com/keigan-motor/pykeigan_motor


==========
ステップ
==========
- KeiganMotor の接続環境に合わせて、まず `USB経由で制御する <usb.html>`_ または `BLE経由で制御する <ble.html>`_ を参照下さい。
- モーターの設定や動作のための API の詳細については、`controller module <pykeigan/controller.html>`_ リファレンスをご覧ください。 

.. note:: BLE は bluepy に依存するため、Linux のみサポートします。

.. tip:: radians から degree など便利な単位変換は、utils module を参照下さい。


.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: コンテンツ:

   install
   usb
   ble
   ./pykeigan/controller
   ./pykeigan/usbcontroller
   ./pykeigan/blecontroller
   ./pykeigan/utils
   examples


==========
リンク
==========

- KeiganMotor ドキュメント: https://docs.keigan-motor.com/


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
