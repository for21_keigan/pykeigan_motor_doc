==========
BLE 経由で制御する
==========
MACアドレス を調べる
-------------
BLEで接続するためには、KeiganMotor の MACアドレス を知る必要があります。
examples ディレクトリにある、KM1Scan.py という シンプルなスクリプトを使用して下さい。
sudo 権限で実行して下さい。

::

    sudo python3 KM1Scan.py

::

    from bluepy.btle import Scanner
    scanner=Scanner()
    devices=scanner.scan(5.0)
    for dev in devices:
        for (adtype, desc, value) in dev.getScanData():
            if desc=="Complete Local Name" and "KM-1" in value:
                print(value,":",dev.addr)

シンプルな動作コード
-------------
KeiganMotor を、反時計回りに 1 rad/sec の速さで回転させます。

::

    from pykeigan import blecontroller
    dev=blecontroller.BLEController("xx:xx:xx:xx:xx")
    dev.enable_action()
    dev.set_speed(1.0)
    dev.run_forward()