==========
USB 経由で制御する
==========
接続パスを調べる
-------------
KeiganMotor に USBで接続するためには、まず KeiganMotor が接続されたパス（ポート）を調べる必要があります。
Linux では、KeiganMotor を USBポートに接続した後、以下のコマンドで、KeiganMotor の一意のパスを知ることができます。

::

    ls /dev/serial/by-id/

KeiganMotor のパスは、このディレクトリに生成されるデバイスファイル名となります。
KeiganMotor KM-1 シリーズでは、接頭辞として、usb-FTDI_FT230X_Basic_UART... という名前が付きます。
例えば、以下のようなものです。
::
    'usb-FTDI_FT230X_Basic_UART_DM00XXXX-if00-port0'

場合によっては、以下のように read/write 権限を与えて下さい。
::
    sudo chmod 666 /dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DM00XXXX-if00-port0

.. note:: Windows では、デバイスマネージャ等を使って COMポート番号を調べて下さい。例えば、'COM3' 等となります。


インスタンスの初期化
-------------
絶対パスを引数にして、usbcontroller インスタンスを初期化します。
::
    dev=usbcontroller.USBContoller('/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DM00xxxx-if00-port0')


シンプルな動作コード
-------------
KeiganMotor を、反時計回りに 1 rad/sec の速さで回転させます。

::

    from pykeigan import usbcontroller
    dev=usbcontroller.USBContoller('/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DM00xxxx-if00-port0')
    dev.enable_action()
    dev.set_speed(1.0)
    dev.run_forward()
