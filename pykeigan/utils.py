# -*- coding: utf-8 -*-

import struct

def float2bytes(float_value):
    """
    float から バイト列に変換します

    Paramters
    -------
    float_value : float

    Returns
    -------
    bytes : bytes array
        バイト列（4バイト）
    """
    float_value=float(float_value)
    return struct.pack("!f", float_value)

def bytes2float(byte_array):
    """
    バイト列 から float に変換します

    Paramters
    -------
    byte_array : bytes array

    Returns
    -------
    float_value : float
    """
    return struct.unpack('!f',byte_array)[0]

def uint8_t2bytes(uint8_value):
    """
    uint8_t から バイト に変換します

    Paramters
    -------
    uint8_value : uint8_t
    
    Returns
    -------
    bytes : bytes array
        バイト列（1バイト）
    """
    uint8_value=int(uint8_value)
    if uint8_value<0:
        raise TypeError("Argument should be positive or equal to zero")
    if uint8_value>256-1:
        raise TypeError("Argument should be less than 256")
    return struct.pack("B",uint8_value)

def uint16_t2bytes(uint16_value):
    """
    uint16_t から バイト に変換します

    Paramters
    -------
    uint16_value : uint16_t

    Returns
    -------
    bytes : bytes array
        バイト列（2バイト）
    """
    uint16_value=int(uint16_value)
    if uint16_value<0:
        raise TypeError("Argument should be positive or equal to zero")
    if uint16_value>256**2-1:
        raise TypeError("Argument should be less than 256**2")
    val1=int(uint16_value/256)
    val2=uint16_value-val1*256
    return struct.pack("BB",val1,val2)

def uint32_t2bytes(uint32_value):
    """
    uint32_t から バイト に変換します

    Paramters
    -------
    uint32_value : uint32_t

    Returns
    -------
    bytes : bytes array
        バイト列（4バイト）
    """
    uint32_value=int(uint32_value)
    if uint32_value<0:
        raise TypeError("Argument should be positive or equal to zero")
    if uint32_value>256**4-1:
        raise TypeError("Argument should be less than 256**4")
    val1=int(uint32_value/256**3)
    val2=int((uint32_value-val1*256**3)/256**2)
    val3=int((uint32_value-val1*256**3-val2*256**2)/256)
    val4=uint32_value-val1*256**3-val2*256**2-val3*256
    return struct.pack("BBBB",val1,val2,val3,val4)

def bytes2uint32_t(ba):
    """
    バイト列 から uint16_t に変換します

    Paramters
    -------
    ba : bytes array

    Returns
    -------
    value : uint32_t
    """
    return struct.unpack(">I",ba)[0]

def bytes2uint16_t(ba):
    """
    バイト列 から uint16_t に変換します

    Paramters
    -------
    ba : bytes array

    Returns
    -------
    value : uint16_t
    """
    return struct.unpack(">H", ba)[0]

def bytes2uint8_t(ba):
    """
    バイト列 から uint8_t に変換します

    Paramters
    -------
    ba : bytes array

    Returns
    -------
    value : uint8_t
    """
    return struct.unpack("B",ba)[0]

def bytes2int16_t(ba):
    """
    バイト列 から int16_t に変換します

    Paramters
    -------
    ba : bytes array

    Returns
    -------
    value : uint16_t
    """   
    return struct.unpack(">h",ba)[0]

def deg2rad(degree):
    """
    degree[度] から radians[rad] に変換します

    Paramters
    -------
    degree : float

    Returns
    -------
    radians : float
    """       
    return degree*0.017453292519943295

def rad2deg(radian):
    """
    radians[rad] から degree[度] に変換します

    Paramters
    -------
    radians : float    

    Returns
    -------
    degree : float
    """       

    return radian/ 0.017453292519943295

def rpm2rad_per_sec(rpm):
    """
    rotation per minute[rpm] から radians per second[rad/sec] に変換します

    Paramters
    -------
    rotation_per_minute : float    

    Returns
    -------
    radian_per_sec : float
    """   
    return rpm *0.10471975511965977

def rad_per_sec2rpm(radian_per_sec):
    """
    radians per second[rad/sec] から rotation per minute[rpm] に変換します

    Paramters
    -------
    radian_per_sec : float    

    Returns
    -------
    rotation_per_minute : float
    """   
    return radian_per_sec/0.10471975511965977
