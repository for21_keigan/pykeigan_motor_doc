# -*- coding:utf-8 -*-

"""
Python Library for Keigan Motor(v2)


Keigan Motor用のPythonライブラリ　https://www.keigan-motor.com/

このライブラリはv1からv2に更新されました。

v1は次のURLから入手できます。https://github.com/keigan-motor/pykeigan_motor/tree/v1

pykeigan_motorではUSBシリアル・BLEを用いてKeigan Motorを制御できます。

現在、このライブラリのBLE機能はbluepy(Python interface to Bluetooth LE on Linux by Mr. Ian Harvey)
に依存しているため、Linuxのみでサポートしています。https://github.com/IanHarvey/bluepy

USBシリアルは、WindowsおよびMacもサポートしています。
"""

__author__ = "keita.yamada"
__version__ = "0.0.1"
__date__    = "13 December 2019"

# DATA_TEST = 'data'

class Requirement:
    """
    ・Python >= 3.5(推奨) または2.6
    ・Pyserial >= 3.4
    ・bluepy >= 1.14(BLEサポート・Linuxのみ)
    """
    
class Usage:
    """
    ・KeiganMotorファームウェア version 2.23 以上
    ・現時点でサポートされているOSはRaspbian(Raspberry Pi),Linux,Windows,MacOSです。ただし、BLE機能はRaspbian(Raspberry Pi),Linuxのみ
    """
    
class Installation:
    def Install_from_source():
        """
        $ sudo apt install git
        $ git clone https://github.com/keigan-motor/pykeigan_motor
        $ cd pykeigan_motor
        $ python setup.py install
        ## and to install BLE Support
        $ pip install .[ble]
        """
        pass
        
    def Install_from_PyPl():
        """
        $ pip install pykeigan-motor
        ## or to install BLE Support
        $ pip install pykeigan-motor[ble]
        """
        pass
        
class USB_Serial:
    """
    Keigan MotorをUSBシリアル経由で接続するには、マウントされたパスを知る必要があります。
    以下の方法で、Keigan Motorの一意のパスを取得できます。
    
    $ ls /dev/serial/by-id/
    
    Keigan MotorのIDは、「 usb-FTDI_FT230X_Basic_UART_DM00XXXX-if00-port0」のようになります。
    USBシリアル経由で、Keigan Motorを使用するには、R/W許可を追加する必要があります。
    
    $ sudo chmod 666 /dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DM00XXXX-if00-port0
    
    """
    
    def example_code():
        """
        ・1.0rad/secで反時計回りに回転します。
        
        from pykeigan import usbcontroller
        dev = usbcontroller.USBController('/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DM00XXXX-if00-port0')
        dev.enable_action()
        dev.set_speed(1.0)
        dev.run_forward()
        """
        pass
    
    def examples():
        """
        ・examples/usb-simple-connection.py
            モーターへの基本的な接続。
            
        ・examples/usb-rotate-the-motor.py
            モーターを連続的に回転させて停止します。
            
        ・examples/usb-position-control.py
            モーターを相対位置と絶対位置に回転させます。
            
        ・examples/usb-get-motor-Informations.py
            モーターの速度、位置、トルク、およびIMU値を取得します。
            
        ・examples/usb-actuator.py
            モーターを移動させて、特定の距離だけ戻します。
            
        ・examples/usb-torque-control.py
            トルク制御のデモ。手でモーターを回転させながらトルクを上げます。
            
        ・examples/usb-teaching-control.py
            モーターにモーションを記録および再生させます。
        """
    
class BLE_For_Linux_Only:
    """
    BLEはLinuxのみです。
    BLE接続を行うには、Keigan MotorのMACアドレスを取得する必要があります。
    次の簡単なスクリプトで使用できます。sudoで実行してください。
    
    KM1Scan.py
    
    from bluepy.btle import Scanner
    scanner=Scanner()
    devices=scanner.scan(5.0)
    for dev in devices:
        for (adtype, desc, value) in dev.getScanData():
            if desc=="Complete Local Name" and "KM-1" in value:
                print(value,":",dev.addr)
            
    """
    
    def example_code():
        """
        ・1.0rad/secで反時計回りに回転します。
        
        from pykeigan import blecontroller 
        dev = blecontroller.BLEController（" xx：xx：xx：xx：xx "）
        dev.enable_action（）
        dev.set_speed（1.0）
        dev.run_forward（）
        
        """
        
    def examples():
        """
        ・examples/ble-simple-connection.py
            モーターへの基本的な接続
            
        ・examples/ble-scanner-connection.py
            BLEスキャナでモーターに接続します。
            
        ・examples/ble-rotate-the-motor.py
            モーターを連続的に回転させて停止します。
            
        ・examples/ble-get-motor-Informations.py
            モーターの速度、位置、トルク、およびIMU値を取得します。
        """

    
if __name__ == '__main__':
    pass
    
