#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thr Jan 10 09:13:24 2018

@author: takata@innovotion.co.jp
@author: harada@keigan.co.jp
"""
import serial,struct,threading,atexit,time
from pykeigan.utils import *

class Controller:
    def __init__(self):
        pass

    def _run_command(self,val,characteristics):
        pass

    @property
    def flash_memory_states(self):
        """
        フラッシュメモリの状態を表します。
        """
        return {0:"FLASH_STATE_READY",1:"FLASH_STATE_TEACHING_PREPARE",2:"FLASH_STATE_TEACHING_DOING",3:"FLASH_STATE_PLAYBACK_PREPARE",4:"FLASH_STATE_PLAYBACK_DOING",5:"FLASH_STATE_PLAYBACK_PAUSING",6:"FLASH_STATE_TASKSET_RECORDING",7:"FLASH_STATE_TASKSET_DOING",8:"FLASH_STATE_TASKSET_PAUSING",20:"FLASH_STATE_IMU"}

    @property
    def motor_control_modes(self):
        """
        モーターの制御モードを表します。
        """
        return {0:"MOTOR_CONTROL_MODE_NONE",1:"MOTOR_CONTROL_MODE_VELOCITY",2:"MOTOR_CONTROL_MODE_POSITION",3:"MOTOR_CONTROL_MODE_TORQUE",0xFF:"MOTOR_CONTROL_MODE_OTHERS"}
    @property
    def error_codes(self):
        """
        モーターのエラーコードを表します。
        """
        return {0x00:"KM_SUCCESS", 0x03:"KM_ERROR_INTERNAL", 0x04:"KM_ERROR_NO_MEM", 0x05:"KM_ERROR_NOT_FOUND", 0x06:"KM_ERROR_NOT_SUPPORTED", 0x07:"KM_ERROR_INVALID_PARAM", 0x08:"KM_ERROR_INVALID_STATE", 0x09:"KM_ERROR_INVALID_LENGTH", 0x0B:"KM_ERROR_INVALID_DATA", 0x0D:"KM_ERROR_TIMEOUT", 0x0E:"KM_ERROR_NULL", 0x0F:"KM_ERROR_FORBIDDEN", 0x10:"KM_ERROR_INVALID_ADDR",0x11:"KM_ERROR_BUSY",0x14:"KM_ERROR_MOTOR_DISABLED",0x15:"KM_ERROR_MOTOR_OVER_TEMPERATURE", 0x64:"KM_SUCCESS_ARRIVAL"}
    @property
    def command_names(self):
        """
        コマンドの名称を表します。
        """
        return {0x00:"unknown",0x02:"set_max_speed",0x03:"set_min_speed",0x05:"set_curve_type",0x07:"set_acc",0x08:"set_dec",0x0e:"set_max_torque",0x16:"set_teaching_interval",0x17:"set_playback_interval",0x18:"set_qcurrent_p",0x19:"set_qcurrent_i",0x1A:"set_qcurrent_d",0x1B:"set_speed_p",0x1C:"set_speed_i",0x1D:"set_speed_d",0x1E:"set_position_p",0x1F:"set_position_i",0x20:"set_position_d",0x21:"set_pos_control_threshold",0x22:"reset_all_pid",0x2C:"set_motor_measurement_interval",0x2D:"set_motor_measurement_settings",0x2E:"set_interface",0x3A:"set_own_color",0x3C:"set_imu_measurement_interval",0x3D:"set_imu_measurement_settings",0x40:"read_register",0x41:"save_all_registers",0x46:"read_device_name",0x47:"read_device_info",0x4E:"reset_register",0x4F:"reset_all_registers",0x50:"disable_action",0x51:"enable_action",0x58:"set_speed",0x5A:"preset_position",0x5B:"get_position_offset",0x60:"run_forward",0x61:"run_reverse",0x62:"run_at_velocity",0x65:"move_to_pos",0x66:"move_to_pos",0x67:"move_by_dist",0x68:"move_by_dist",0x6C:"free_motor",0x6D:"stop_motor",0x72:"hold_torque",0x75:"move_to_pos_until_arrival",0x76:"move_to_pos_until_arrival",0x77:"move_by_pos_until_arrival",0x78:"move_by_pos_until_arrival",0x81:"start_doing_taskset",0x82:"stop_doing_taskset",0x85:"start_playback_motion",0x86:"prepare_playback_motion",0x87:"start_playback_motion_from_prep",0x88:"stop_playback_motion",0x90:"pause_queue",0x91:"resume_queue",0x92:"wait_queue",0x94:"erase_task",0x95:"clear_queue",0x9A:"get_status",0xA0:"start_recording_taskset",0xA2:"stop_recording_taskset",0xA3:"erase_taskset",0xA4:"erase_all_tasksets",0xA5:"set_taskset_name",0xA6:"read_taskset_info",0xA7:"read_taskset_usage",0xA9:"start_teaching_motion",0xAA:"prepare_teaching_motion",0xAB:"start_teaching_motion_from_prep",0xAC:"stop_teaching_motion",0xAD:"erase_motion",0xAE:"erase_all_motions",0xAF:"set_motion_name",0xB0:"read_motion_info",0xB1:"read_motion_usage",0xB4:"get_motor_measuremet",0xB5:"get_imu_measurement"
            ,0xB7:"read_motion" ,0xB8:"write_motion_position",0xE0:"set_led",0xE6:"enable_motor_measurement",0xE7:"disable_motor_measurement",0xEA:"enable_imu_measurement",0xEB:"disable_imu_measurement",0xF0:"reboot"}

    """
    ==========
    Functions
    ==========
    """

    def set_max_speed(self,max_speed,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの最大速さを[rad/sec]で設定します。

        Parameters
        ----------
        max_speed : float 
            最大速さ [radian / second]（正の値）
        
        identifier : int
        
        crc16 : int
        """

        command=b'\x02'
        values=float2bytes(max_speed)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_min_speed(self,min_speed,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの最小速さを[rad/sec]で設定します。
        
        Parameters
        ----------
        min_speed : float 
            最小速さ [radian / second] (正の値)
            
        indentifier : int
        
        crc16 : int
        """

        command=b'\x03'
        values=float2bytes(min_speed)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_curve_type(self,curve_type,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        加減速曲線を設定します(モーションコントロールの設定)。
        
        Parameters
        ----------
        curve_type : int 加減速カーブオプション
        
            typedef enum curveType =
            {
                CURVE_TYPE_NONE = 0, // Turn off Motion control
                CURVE_TYPE_TRAPEZOID = 1, // Turn on Motion control with trapezoidal curve
            }
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x05'
        values=uint8_t2bytes(curve_type)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_acc(self,_acc,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの加速度を[rad/sec^2]で設定します。
        
        Parameters
        ----------
        _acc : float
            加速度 0-200 [radian / second^2] (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x07'
        values=float2bytes(_acc)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_dec(self,_dec,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの減速度を[rad/sec^2]で設定します。
        decは、モーションコントロールONの場合、減速時に使用されます。(減速時の直線の傾き)
        
        Parameters
        ----------
        _dec : float
            減速度 0-200 [radian / second^2] (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x08'
        values=float2bytes(_dec)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_max_torque(self,max_torque,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの最大トルク(絶対値)を[N*m]で設定します。

        max_torqueを設定することにより、トルクの絶対値がmax_torqueを超えないように運転します。
        max_torque = 0.1[Nm]の後にrun_forward(正回転)を行った場合、0.1[Nm]を超えないようにその速度をなるだけ維持します。
        ただし、トルクの最大値制限により、システムによっては制御性(振動)が悪化する可能性があります。
        
        Parameters
        ----------
        max_torque : float
            最大トルク [N*m] (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x0E'
        if max_torque < 0:
            raise ValueError("Value out of range")
        values=float2bytes(max_torque)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_teaching_interval(self,interval_ms,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        ティーチングのサンプリング間隔を設定します[ms]
        
        Parameters
        ----------
        interval_ms : float
            ms (2-1000 0, 1msはエラーになる)
        
        identifier : int
        
        crc16 : int
        """
        command=b'\x16'
        values=uint32_t2bytes(interval_ms)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_playback_interval(self,interval_ms,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーション再生時の再生間隔を設定します[ms]
        
        Parameters
        ----------
        interval_ms : float
            ms (2-1000 0, 1msはエラーになる)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x17'
        values=uint32_t2bytes(interval_ms)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_qcurrent_p(self,q_current_p,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターのq軸電流PIDコントローラのP(比例)ゲインを設定します。
        
        Parameters
        ----------
        q_current_p : float
            q電流Pゲイン (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x18'
        values=float2bytes(q_current_p)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_qcurrent_i(self,q_current_i,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターのq軸電流PIDコントローラのI(積分)ゲインを設定します。
        
        Parameters
        ----------
        q_current_i : float
            q電流Iゲイン (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x19'
        values=float2bytes(q_current_i)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_qcurrent_d(self,q_current_d,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターのq軸電流PIDコントローラのD(微分)ゲインを設定します。
        
        Parameters
        ----------
        q_current_d : float
            q電流Dゲイン (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x1A'
        values=float2bytes(q_current_d)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_speed_p(self,speed_p,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの速度PIDコントローラのP(比例)ゲインを設定します。
        
        Parameters
        ----------
        speed_p : float
            速度Pゲイン (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x1B'
        values=float2bytes(speed_p)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_speed_i(self,speed_i,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの速度PIDコントローラのI(積分)ゲインを設定します。
        
        Parameters
        ----------
        speed_i : float
            速度Iゲイン (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x1C'
        values=float2bytes(speed_i)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_speed_d(self,speed_d,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの速度PIDコントローラのD(微分)ゲインを設定します。
        
        Parameters
        ----------
        speed_d : float
            速度Dゲイン (正の値)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x1D'
        values=float2bytes(speed_d)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_position_p(self,position_p,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの位置PIDコントローラのP(比例)ゲインを設定します。
        
        Parameters
        ----------
        position_p : float
            位置Pゲイン [1.0000 - 20.0000] (デフォルト 5.0)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x1E'
        values=float2bytes(position_p)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_position_i(self,position_i,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの位置PIDコントローラのI(積分)ゲインを設定します。
        
        Parameters
        ----------
        position_i : float
            位置Iゲイン [1.0000 - 100.0000] (デフォルト 10.0)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x1F'
        values=float2bytes(position_i)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_position_d(self,position_d,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの位置PIDコントローラのD(微分)ゲインを設定します。
        
        Parameters
        ----------
        position_d : float
            位置Dゲイン [0.0001 - 0.2] (デフォルト 0.01)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x20'
        values=float2bytes(position_d)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_pos_control_threshold(self,poscontrol_d,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの位置制御時、ID制御を有効にする偏差（現在位置と目標位置との差）の絶対値を指定します。

        偏差が閾値を超えている間は、Iゲイン、Dゲインは0に設定されます。
        常にPID制御を有効にするためには、閾値を十分大きく取って下さい。
        
        Parameters
        ----------
        poscontrol_d : float
            [0.0 - n] (デフォルト 0.0139626f // 0.8deg)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x21'
        values=float2bytes(poscontrol_d)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def reset_all_pid(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        全てのPIDパラメータをリセットしてファームウェアの初期設定に戻す。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x22'
        self._run_command(command+identifier+crc16,'motor_rx')

    def set_motor_measurement_interval(self,interval,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーター測定値の取得間隔を設定します。

        有線（USB, I2C）のみ有効。BLEでは固定 100ms 間隔で通知されます。
        
        Parameters
        ----------
        interval : int #確認　JS版ではenum
            モーターの計測値の取得間隔
        
        identifier : int
        
        crc16 : int
        """
        
        command=b'\x2C'
        values = uint8_t2bytes(interval)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_motor_measurement_settings(self,flag,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーター測定値の通知設定を設定します。

        -----------
        bit7: -
        bit6: -
        bit5: -
        bit4: -
        bit3: -
        bit2: -
        bit1: Notification of motor measurement (1:ON, 0:OFF)
        bit0: To start Notification of motor measurement when booting(1:ON, 0:OFF)
        -----------
        
        Parameters
        ----------
        flag : int #確認　
        
        identifier : int
        
        crc16 : int
        """
        command=b'\x2D'
        values=uint8_t2bytes(flag)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    @property
    def interface_type(self):
        return {
            "BLE": 0b1,
            "USB": 0b1000,
            "I2C": 0b10000,
            "BTN": 0b10000000,
        }

    def set_interface(self,flag,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        データインターフェイス(3つの物理ボタン、I2C、USB及びBLE)を有効または無効にすることができます。
        
        モーターの測定値とIMU値の出力インターフェイスを次のように選択します(デフォルト)。
        (高優先度) BLE > USB > I2C (低優先度)
        USB経由で強制的に測定値を送信する場合は、
        bit0(BLE)をOFF(0)、bit3(USB)をON(1)に設定する必要があります。
        たとえば、set_interface(0b10001000)を呼び出す場合、3つの物理ボタン：有効、I2C：無効、USB：有効、BLE：無効
        
        この設定をフラッシュメモリに保存する場合は、必ずsave_all_registers()を呼び出してください。
        
        -----------
        bit7: Physical 3 buttons
        bit6: -
        bit5: -
        bit4: I2C(Wired)
        bit3: USB(Wired)
        bit2: -
        bit1: -
        bit0: BLE(Wireless)
        -----------
        
        Parameters
        ----------
        flag : int #確認
        
        identifier : int
        
        crc16 : int
        """
        command=b'\x2E'
        values=uint8_t2bytes(flag)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_own_color(self,red,green,blue,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの固有LEDカラーを設定します。
        
        Parameters
        ----------
        red : int
            0 - 255
            
        green : int
            0 - 255
            
        blue : int
            0 - 255
        
        identifier : int
        
        crc16 : int
        """
        command=b'\x3A'
        values=uint8_t2bytes(red)+uint8_t2bytes(green)+uint8_t2bytes(blue)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_imu_measurement_interval(self,interval,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        IMU測定値を取得する間隔を設定します。

        有線接続(USB,I2C)でのみ有効です。
        
        Parameters
        ----------
        interval : int #確認
        
        identifier : int
        
        crc16 : int
        """
        command=b'\x3C'
        values = uint8_t2bytes(interval)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def set_imu_measurement_settings(self,flag,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        IMUの通知設定を設定します。
        -----------
        bit7: -
        bit6: -
        bit5: -
        bit4: -
        bit3: -
        bit2: -
        bit1: Notification of IMU (1:ON, 0:OFF)
        bit0: To start Notification of IMU when booting(1:ON, 0:OFF)
        -----------
        
        Parameters
        ----------
        flag : int #確認
        
        identifier : int
        
        crc16 : int
        """
        command=b'\x3D'
        values=uint8_t2bytes(flag)
        self._run_command(command+identifier+values+crc16,'motor_rx')


    def read_register(self,register,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        '''
        指定された設定(register)を読み取ります。
        
        Parameters
        ----------
        register : int　#確認
            #確認
        
        identifier : int
        
        crc16 : int
        '''
        command=b'\x40'
        values=uint8_t2bytes(register)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def save_all_registers(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        すべての設定値(レジスタ)をフラッシュメモリに保存します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x41'
        self._run_command(command+identifier+crc16,'motor_rx')

    def reset_register(self,register,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        指定したレジスタをファームウェアの初期値にリセットします。
        
        Parameters
        ----------
        register : int #確認
            初期値にリセットするコマンド(レジスタ)値
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x4E'
        values=uint8_t2bytes(register)
        self._run_command(command+identifier+values+crc16,'motor_rx')

    def reset_all_registers(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        全てのレジスタをファームウェアの初期値にリセットします。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x4F'
        self._run_command(command+identifier+crc16,'motor_rx')

    # Motor Action
    def disable_action(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーター動作を不許可とします(上位命令)

        安全用：この命令を入れるとモーターは動作しません
        コマンドはタスクセットに記録することは不可です
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x50'
        self._run_command(command+identifier+crc16,'motor_tx')

    def enable_action(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの動作を許可します(上位命令)

        安全用：この命令を入れるとモーターは動作可能となります
        モーター起動時は disable 状態のため、本コマンドで動作を許可する必要があり、
        本コマンドはタスクセットに記録することは不可です
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x51'
        self._run_command(command+identifier+crc16,'motor_tx')

    def set_speed(self,speed,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        速さをセットします（単位系：ラジアン）
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x58'
        values=float2bytes(speed)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def preset_position(self,position,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        位置のプリセットを行います(原点設定)(単位系：ラジアン)
        
        これにより、現在地点が position の座標とみなされます（値がオフセットされます）

        Parameters
        ----------
        position : float
            絶対角度：radians
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x5A'
        values=float2bytes(position)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def get_position_offset(self,position,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        位置のオフセット値を取得します。

        preset_positionで設定した値に対応します。
        
        Parameters
        ----------
        position : float
            絶対角度：radians
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x5B'
        values=float2bytes(position)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def run_forward(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        設定した速さで正回転(反時計回り)させます
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x60'
        self._run_command(command+identifier+crc16,'motor_tx')

    def run_reverse(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        設定した速さで逆回転(時計回り)させます
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x61'
        self._run_command(command+identifier+crc16,'motor_tx')

    def run_at_velocity(self,velocity,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        velocityで設定された速度でモーターを回転させます

        velocityは正負可
        
        Parameters
        ----------
        velocity : float
            速度の大きさ [rad / sec]
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x62'
        values=float2bytes(velocity)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def move_to_pos(self,position,speed=None,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        speedで設定された速度で絶対位置に移動します。

        speedがNoneの場合は、0x58で設定された速度で移動します。
        
        Parameters
        ----------
        position : float
            角度：radians
            
        speed : float
            速度の大きさ [rad / sec]
            
        identifier : int
        
        crc16 : int
        """
        if speed is not None:
            command=b'\x65'
            values=float2bytes(position)+float2bytes(speed)
        else:
            command=b'\x66'
            values=float2bytes(position)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def move_by_dist(self,distance,speed=None,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        speedで設定された速度で相対位置に移動します。

        speedがNoneの場合は、0x58で設定された速度で移動します。
        
        Parameters
        ----------
        position : float
            角度：radians[左：+radians 右：-radians]
            
        speed : float
            速度の大きさ [rad / sec] (正の数)
            
        identifier : int
        
        crc16 : int
        """
        if speed is not None:
            command=b'\x67'
            values=float2bytes(distance)+float2bytes(speed)
        else:
            command=b'\x68'
            values=float2bytes(distance)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def free_motor(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの励磁を停止します
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x6C'
        self._run_command(command+identifier+crc16,'motor_tx')

    def stop_motor(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの速度をゼロまで減速し停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x6D'
        self._run_command(command+identifier+crc16,'motor_tx')

    def hold_torque(self,torque,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        指定されたトルクでトルク制御を行います。

        速度や位置を同時に制御する場合は、set_max_torque と run_forward 等を併用してください。
        
        Parameters
        ----------
        torque : float
            トルク 単位：N・m [-X ~ +X Nm] 推奨値 0.3-0.05
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x72'
        values=float2bytes(torque)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    # def move_to_pos_until_arrival(self,position,speed=None,identifier=b'\x00\x00',crc16=b'\x00\x00'):
    #     """
    #     todo::1.86 farmBug
    #     Move to the absolute 'position' at the 'speed'. If the speed is None, move at the speed set by 0x58: set_speed. This command is active until arriving the position. The next command execution waits in a queue.
    #     """
    #     if speed is not None:
    #         command=b'\x75'
    #         values=float2bytes(position)+float2bytes(speed)
    #     else:
    #         command=b'\x76'
    #         values=float2bytes(position)
    #     self._run_command(command+identifier+values+crc16,'motor_tx')

    # def move_by_dist_until_arrival(self,position,speed=None,identifier=b'\x00\x00',crc16=b'\x00\x00'):
    #     """
    #     todo::1.86 farmBug
    #     Move the motor by the specified relative 'distance' from the current position at the 'speed'. If the speed is None, move at the speed set by 0x58: set_speed. This command is active until arriving the position. The next command execution waits in a queue.
    #     """
    #     if speed is not None:
    #         command = b'\x77'
    #         values = float2bytes(position) + float2bytes(speed)
    #     else:
    #         command = b'\x78'
    #         values = float2bytes(position)
    #
    #     self._run_command(command+identifier+values+crc16,'motor_tx')

    def start_doing_taskset(self,index,repeating,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        記憶したタスク(命令)のセットを実行します。

        KM-1は、index: 0~49 まで。（50個のメモリバンク 各8128 Byte まで制限あり）
        タスクセットの記録は、コマンド（タスクセット）を参照下さい。
        # URL変更
        
        Parameters
        ----------
        index : int
            タスクセット番号(0-65535)
        
        repeating : int
            繰り返し回数 0は無制限
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x81'
        values=uint16_t2bytes(index)+uint32_t2bytes(repeating)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def stop_doing_taskset(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        現在のタスクセットの実行を停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x82'
        self._run_command(command+identifier+crc16,'motor_tx')

    def start_playback_motion(self,index,repeating,option,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーションを再生します(準備なし)

        モーションのプレイバックを(準備なしで)プレイバック開始します。
        
        Parameters
        ----------
        index : int
            モーション番号(0-65535)
        
        repeating : int
            繰り返し回数 0は無制限
            
        option : int
            スタート位置の設定
            START_POSITION_ABS：記憶された開始位置（絶対座標）からスタート
            START_POSITION_CURRENT：現在の位置を開始位置としてスタート
            # ↑このままでいいか
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x85'
        values=uint16_t2bytes(index)+uint32_t2bytes(repeating)+uint8_t2bytes(option)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def prepare_playback_motion(self,index,repeating,option,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーションを再生します(準備あり)

        モーションのプレイバックを(準備ありで)プレイバック開始します。
        
        Parameters
        ----------
        index : int
            モーション番号(0-65535)
        
        repeating : int
            繰り返し回数 0は無制限
            
        option : int
            スタート位置の設定
            START_POSITION_ABS：記憶された開始位置（絶対座標）からスタート
            START_POSITION_CURRENT：現在の位置を開始位置としてスタート
            # ↑このままでいいか
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x86'
        values=uint16_t2bytes(index)+uint32_t2bytes(repeating)+uint8_t2bytes(option)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def start_playback_motion_from_prep(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        最後のprepare_playback_motionの状態でモーションの再生を開始します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x87'
        self._run_command(command+identifier+crc16,'motor_tx')

    def stop_playback_motion(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        プレイバックモーションを停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x88'
        self._run_command(command+identifier+crc16,'motor_tx')

    # Queue
    def pause_queue(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        再開が実行されるまで(0x91)、キューを一時停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x90'
        self._run_command(command+identifier+crc16,'motor_tx')

    def resume_queue(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        キューを再開します(蓄積されたタスク処理を再開)
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x91'
        self._run_command(command+identifier+crc16,'motor_tx')

    def wait_queue(self,waittime,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        キューを指定時間停止し再開します。

        pause_queueを実行し、指定時間(ミリ秒)経過後、自動的にresume_queueを行います。
        
        Parameters
        ----------
        waittime : int
            停止時間[msec]
            
        identifier : int
        
        crc16 : int
        """
        command=b'\x92'
        values=uint32_t2bytes(waittime)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def clear_queue(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        キューをリセットします。

        キュー内の全てのタスクを消去します。
        このコマンドは、pause_queueまたはwait_queueが実行された時に機能します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\x95'
        self._run_command(command+identifier+crc16,'motor_tx')

    # Taskset
    def start_recording_taskset(self,index,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        タスク(命令)のセットの記録を開始します。

        記憶するインデックスのメモリはコマンド:erase_tasksetにより予め消去されている必要があります。
        
        Parameters
        ----------
        index : int
            KM-1の場合、インデックスの値は0-49(計50個記憶)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\xA0'
        values=uint16_t2bytes(index)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def stop_recording_taskset(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        タスクセットの記録を停止します。

        このコマンドは、start_recording_tasksetの実行中に機能します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xA2'
        self._run_command(command+identifier+crc16,'motor_tx')

    def erase_taskset(self,index,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        指定のインデックスのタスクセットを消去します。
        
        Parameters
        ----------
        index : int
            KM-1の場合、インデックスの値は0-49(計50個記憶)
            
        identifier : int
        
        crc16 : int
        """
        command=b'\xA3'
        values=uint16_t2bytes(index)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def erase_all_tasksets(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        全てのタスクセットを消去します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xA4'
        self._run_command(command+identifier+crc16,'motor_tx')

    # Teaching
    def start_teaching_motion(self,index,time_ms,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        ティーチングを開始します(準備なし)

        In the case of KM-1, index value is from 0 to 9 (10 in total).  Recording time cannot exceed 65408 [msec].
        KM-1の場合、インデックスの値は0～19（計20個記録）となる。記録時間は65408 [msec] を超えることはできない。
        # インデックスの値　JS版と違う
        
        Parameters
        ----------
        index : int
            インデックス [0-19]
            
        time_ms : int
            記録時間 [msec 0-65408]
            
        identifier : int
        
        crc16 : int
        """
        command=b'\xA9'
        values=uint16_t2bytes(index)+uint32_t2bytes(time_ms)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def prepare_teaching_motion(self,index,time_ms,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        ティーチングを開始します(準備状態から)

        In the case of KM-1, index value is from 0 to 9 (10 in total).  Recording time cannot exceed 65408 [msec].
        KM-1の場合、インデックスの値は0～19（計20個記録）となる。記録時間は65408 [msec] を超えることはできない。
        # インデックスの値　JS版と違う
        
        Parameters
        ----------
        index : int
            インデックス [0-19]
            
        time_ms : int
            記録時間 [msec 0-65408]
            
        identifier : int
        
        crc16 : int
        """
        command=b'\xAA'
        values=uint16_t2bytes(index)+uint32_t2bytes(time_ms)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def start_teaching_motion_from_prep(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        最後のprepare_teaching_motionの状態で、モーションのティーチングを開始します。
        このコマンドは、prepare_teaching_motionで指定されている場合に機能します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xAB'
        self._run_command(command+identifier+crc16,'motor_tx')

    def stop_teaching_motion(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        実行中のティーチングを停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xAC'
        self._run_command(command+identifier+crc16,'motor_tx')

    def erase_motion(self,index,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        指定されたインデックスのモーションを消去します。
        KM-1の場合、インデックス値は0-9(合計10)です。
        In the case of KM-1, index value is from 0 to 9 (10 in total).
        # インデックスの値　JS版と違う
        
        Parameters
        ----------
        index : int
        	インデックス [0-9]
        		
        identifier : int
        
        crc16 : int
        """
        command=b'\xAD'
        values=uint16_t2bytes(index)
        self._run_command(command+identifier+values+crc16,'motor_tx')

    def erase_all_motions(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        全てのモーションを消去します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xAE'
        self._run_command(command+identifier+crc16,'motor_tx')

    def read_motion(self,index,read_comp_cb,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        Externally output the motion stored by direct teaching. (Motor farm ver>=2.0)
        It is valid only for a wired connection(USB).
          callback signature
            read_comp_cb(motion_index,motion_value[])
            
        ダイレクトティーチングにより保存されたモーションを出力します(ファームウェアver>=2.0)
        有線接続(USB)でのみ有効です。
            callback signature
                read_comp_cb(motion_index,motion_value[])
                
        Parameters
        ----------
        index : int
        		インデックス [0-9]
          
        read_comp_cb : 
        # 確認
        		
        identifier : int
        
        crc16 : int
        """
        command=b'\xB7'
        values = uint16_t2bytes(index)
        self._run_command(command + identifier + values + crc16, 'motor_tx')
        self.on_read_motion_read_comp_cb=read_comp_cb
        return True

    def write_motion_position(self,position,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        外部からモーションの座標１つを記録します。(ファームウェアver >= 2.0)
        有線接続(USB)でのみ有効です。
            ※ このコマンドは、prepareTeaching状態でない限り受け入れられません。prepare_teaching_motionの実行後に行うこと。
            ※ このコマンドは、座標を１つずつ記録します。１つの記録が作成されると、次の座標の記録をするのを待っています。
            ※ このコマンドはで座標を記録した後、stop_teaching_motionを実行しないと記録を正常に完了できません。
        
        Parameters
        ----------
        position : float
            角度 : radians
            
        identifier : int
        
        crc16 : int
        """
        command=b'\xB8'
        values = float2bytes(position)
        self._run_command(command + identifier + values + crc16, 'motor_tx')

    # LED
    def set_led(self,ledState,red,green,blue,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        LEDの状態(オフ、点灯、点滅、暗)および色の強度(赤、緑、青)を設定します。
        typedef enum ledState =
        {
            LED_STATE_OFF = 0, // LED off
            LED_STATE_ON_SOLID = 1, // LED solid
            LED_STATE_ON_FLASH = 2, // LED flash
            LED_STATE_ON_DIM = 3 // LED dim
        }
        
        Parameters
        ----------
        ledState : int
            点灯状態
            
        red : int
            0-255
            
        green : int
            0-255
            
        blue : int
            0-255
            
        identifier : int
        
        crc16 : int
        """
        command=b'\xE0'
        values=uint8_t2bytes(ledState)+uint8_t2bytes(red)+uint8_t2bytes(green)+uint8_t2bytes(blue)
        self._run_command(command+identifier+values+crc16,"motor_tx")

    def enable_continual_motor_measurement(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーター測定を有効にし、測定値の継続的な通知を開始します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xE6'
        self._run_command(command+identifier+crc16,'motor_tx')

    def disable_continual_motor_measurement(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        モーターの測定を無効にし、測定値の通知を停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xE7'
        self._run_command(command+identifier+crc16,'motor_tx')

    # IMU
    def enable_continual_imu_measurement(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        IMU(ジャイロ)の値取得(通知)を開始します。
        # 確認　説明文
        本コマンドを実行すると、IMUのデータはBLEのキャラクタリスティクスMOTOR_IMU_MEASUREMENTに通知されます
        MOTOR_IMU_MEASUREMENTのnotifyはイベントタイプ KMMotorCommandKMOne.EVENT_TYPE.imuMeasurement に通知
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xEA'
        self._run_command(command+identifier+crc16,'motor_tx')

    def disable_continual_imu_measurement(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        IMU(ジャイロ)の値取得(通知)を停止します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xEB'
        self._run_command(command+identifier+crc16,'motor_tx')

    # System
    def reboot(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        システムを再起動します。
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xF0'
        self._run_command(command+identifier+crc16,'motor_tx')

    def enter_device_firmware_update(self,identifier=b'\x00\x00',crc16=b'\x00\x00'):
        """
        ファームウェアアップデートモードに入ります。
        ファームウェアをアップデートするためのブートローダーモードに入ります。(システムは再起動される)
        
        Parameters
        ----------
        identifier : int
        
        crc16 : int
        """
        command=b'\xFD'
        self._run_command(command+identifier+crc16,'motor_tx')

    def read_max_speed(self):
        """
        現在の最大回転速度を[red/sec]で読み取ります。
        """
        return self._read_setting_value(0x02)

    def read_min_speed(self):
        """
        現在の最小回転速度を[red/sec]で読み取ります。
        """
        return self._read_setting_value(0x03)

    def read_curve_type(self):
        """
        現在の加減速曲線を読み取ります。
        typedef enum curveType =
        {
            CURVE_TYPE_NONE = 0, // モーションコントロール　OFF
            CURVE_TYPE_TRAPEZOID = 1, // モーションコントロール ON （台形加減速）
        }
        """
        return self._read_setting_value(0x05)

    def read_acc(self):
        """
        現在の加速度を読み取ります。
        """
        return self._read_setting_value(0x07)

    def read_dec(self):
        """
        現在の減速度を読み取ります。
        """
        return self._read_setting_value(0x08)

    def read_max_torque(self):
        """
        現在の最大トルクを読み取ります。
        """
        return self._read_setting_value(0x0E)

    def read_qcurrent_p(self):
        """
        q軸電流PIDコントローラの比例ゲインを読み取ります。
        """
        return self._read_setting_value(0x18)

    def read_qcurrent_i(self):
        """
        q軸電流PIDコントローラの積分ゲインを読み取ります。
        """
        return self._read_setting_value(0x19)

    def read_qcurrent_d(self):
        """
        q軸電流PIDコントローラの微分ゲインを読み取ります。
        """
        return self._read_setting_value(0x1A)

    def read_speed_p(self):
        """
        速度PIDコントローラの比例ゲインを読み取ります。
        """
        return self._read_setting_value(0x1B)

    def read_speed_i(self):
        """
        速度PIDコントローラの積分ゲインを読み取ります。
        """
        return self._read_setting_value(0x1C)

    def read_speed_d(self):
        """
        速度PIDコントローラの微分ゲインを読み取ります。
        """
        return self._read_setting_value(0x1D)

    def read_position_p(self):
        """
        位置PIDコントローラの比例ゲインを読み取ります。
        """
        return self._read_setting_value(0x1E)

    def read_position_i(self):
        """
        位置PIDコントローラの積分ゲインを読み取ります。
        """
        return self._read_setting_value(0x1F)

    def read_position_d(self):
        """
        位置PIDコントローラの微分ゲインを読み取ります。
        """
        return self._read_setting_value(0x20)

    def read_pos_control_threshold(self):
        """
        ターゲットと現在の位置との偏差の閾値を読み取ります。
        偏差が閾値を超えている間、積分ゲインと微分ゲインはゼロに設定されます。
        """
        return self._read_setting_value(0x21)

    def read_own_color(self):
        """
        ToDo
        Read the own LED color. Return (red,green,blue).
        
        モーターのLEDの色を読み取る。(赤、緑、青)を返す。
        """
        return self._read_setting_value(0x3A)

    def read_device_name(self):
        """
        モーターのデバイス名を読み取ります。
        """
        return self._read_setting_value(0x46)

    def read_device_info(self):
        """
        モーターのデバイス情報を取得します。
        """
        return self._read_setting_value(0x47)

    def read_status(self):
        """
        モーターのステータスを読み取ります。
        'isCheckSumEnabled', 'iMUMeasurement', 'motorMeasurement', 'queue', 'motorEnabled', 'flash_memory_state', 'motor_control_mode'.
        """
        return self._read_setting_value(0x9A)

    def _read_setting_value(self, comm):#dummy
        pass
    def _read_motion_value(self):#dummy
        pass
